package helloworld;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class SimpleApplication extends Application<ApplicationConfiguration> {

	public static void main(String[] args) throws Exception {
		new SimpleApplication().run(args);
	}
	
	@Override
	public String getName() {
		return "HelloWorld";
	}
	
	@Override
	public void run(ApplicationConfiguration configuration, Environment env) throws Exception {
        env.jersey().register(new HelloResource());
	}

}
