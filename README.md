# Dropwizard Hello World Example

## About

This is an example project which uses Dropwizard to create a REST service which can be deployed
with Fabric8 on a Kubernetes cluster (or minikube for testing).


## How to start the application locally 

1. Run `mvn clean package` to build your application
2. Start application with `java -jar target/helloworld-1.0.0.jar server`
3. To check that your application is running enter url `http://localhost:8080`


## How to deploy the application on minikube

First you have to start your minikube cluster (`minikube start`). Then use `mvn fabric8:run` to
deploy this application on your cluster.


## How to test the application on minikube

You need to retrieve the IP address of your cluster.

    minikube ip

Then you need to identify the port which has been assigned to your REST service.

    kubectl get services
 
